import dotenv from 'dotenv';
import server from './src/server';
import {LogError, LogSuccess} from './src/utils/logger'


// Configuration the .env file
dotenv.config();

const port = process.env.PORT || 8000;

server.listen(port, () =>{
    LogSuccess(`[SERVER ON] Server running on port ${port}`)
});

server.on('error', (error) => {
    LogError('Error')
})