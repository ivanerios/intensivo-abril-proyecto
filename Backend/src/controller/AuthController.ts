import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IAuthController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";
import { IUser } from "../domain/interfaces/IUser.interface";
import { IAuth } from "../domain/interfaces/IAuthInterface";

import { registerUser, loginUser, logoutUser } from "../domain/orm/Auth.orm";
import { AuthResponse, ErrorResponse } from "./types";
import { GetAllUsers, GetUserByID } from "../domain/orm/User.orm";

@Route("/api/auth")
@Tags("AuthController")
export class AuthController implements IAuthController {

    @Post("/register")
    public async registerUser(user: IUser): Promise<any> {

        let response: any = '';

        if (user) {
            LogSuccess('[/api/auth/register] Register a New User Request')
            await registerUser(user).then((r) => {
                response = {
                    message: `User created successfully with name: ${user.name}`
                }
            });
        } else {
            LogWarning('[/api/auth/register] Register needs Auth Entity (email && password)')
            response = {
                message: 'User not registered: Please, provide a User Entity to create one'
            }
        }

        return response;
    }

    @Post("/login")
    public async loginUser(auth: IAuth): Promise<any> {

        let response: AuthResponse | ErrorResponse | undefined;

        if (auth) {
            LogSuccess('[/api/auth/login] Login a User Request')
            let data = await loginUser(auth);
            response = {
              _id: data.user._id,
              name: data.user.name,
              role: data.user.role,
              email: data.user.email,
              age: data.user.age,
              katas: data.user.katas,
                token: data.token,
                message: `Welcome, ${data.user.name}`
            }
        } else {
            LogWarning('[/api/auth/login] Login needs Auth Entity (email && password)')
            response = {
                message: '[AUTH ERROR] Please, provide a email && password to login'
            }
        }

        return response;
    }

    @Get("/me")
  public async userData(@Query() id: string): Promise<any> {

    let response: any = '';

    if (id) {
      LogSuccess('[/api/users] Get User by ID Request')
      response = await GetUserByID(id);
    }

    return response;


  }

    @Post("/logout")
    logoutUser(): Promise<any> {

        let response: any = '';

        throw new Error("Method not implemented.");
    }


}