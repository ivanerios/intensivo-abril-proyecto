import { IAuth } from "src/domain/interfaces/IAuthInterface";
import { IKata } from "src/domain/interfaces/IKata.interface";
import { IUser } from "src/domain/interfaces/IUser.interface";
import { BasicResponse } from "../types";

export interface IHelloController {
    getMessage(name?:string): Promise<BasicResponse>
}

export interface IUserController {
    getUsers(page?: number, limit?: number, id?: string): Promise<any>
   // getKatas(page: number, limit: number, id?: string): Promise<any>
    deleteUser(id?: string): Promise<any>
    updateUser(id: string, user: IUser): Promise<any>
}

export interface IAuthController {
    registerUser(user: IUser): Promise<any>
    loginUser(auth: IAuth): Promise<any>
    logoutUser(): Promise<any>
}

export interface IKataController {
    getKatas(page: number, limit: number, id?: string, creator?: string, participant?: string, sort?: string): Promise<any>
    createKata(kata: IKata): Promise<any>
    deleteKata(id: string): Promise<any>
    updateKata(id: string, kata: IKata): Promise<any>
}