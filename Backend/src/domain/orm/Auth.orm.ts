import { userEntity } from "../entitites/User.entity";
import { LogSuccess, LogError } from "../../utils/logger";
import { IUser } from "../interfaces/IUser.interface";
import { IAuth } from "../interfaces/IAuthInterface";

import dotenv from 'dotenv';

import bcrypt from 'bcrypt';

import jwt from 'jsonwebtoken';

dotenv.config();

const secret = process.env.SECRETKEY || 'MYSECRETKEY';

// Register User

export const registerUser = async (user: IUser): Promise<any | undefined> => {
  try {
    let userModel = userEntity();
    return await userModel.create(user)
  } catch (error) {
    LogError(`[ORM ERROR]: Creating User: ${error}`);
  }
}

// Login User

export const loginUser = async (auth: IAuth): Promise<any | undefined> => {
  try {
    let userModel = userEntity();
    let userFound: IUser | undefined = undefined;
    let token = undefined;

    await userModel.findOne({ email: auth.email }).then((user: IUser) => {
      userFound = user;
    }).catch((error) => {
      console.error('[ERROR Authentication in ORM] User Not Found');
      throw new Error(`[ERROR Authentication in ORM] User Not Found: ${error}`)
    });

    let validPassword = bcrypt.compareSync(auth.password, userFound!.password);

    if (!validPassword) {
      console.error('[ERROR Authentication in ORM] Password no valid');
      throw new Error(`[ERROR Authentication in ORM] Password no valid`)
    }

    token = jwt.sign({ email: userFound!.email }, secret, {
      expiresIn: "3h"
    });

    return {
      user: userFound,
      token: token
    }


  } catch (error) {
    LogError(`[ORM ERROR]: Creating User: ${error}`);
  }
}

//Logout User

export const logoutUser = async (): Promise<any | undefined> => {

}