import { userEntity } from "../entitites/User.entity";
import { LogSuccess, LogError } from "../../utils/logger";
import { IUser } from "../interfaces/IUser.interface";
import { UserResponse } from "../types/UserResponse.type";
import { kataEntity } from "../entitites/Kata.entity";
import { IKata } from "../interfaces/IKata.interface";
import mongoose from "mongoose";

import dotenv from 'dotenv';

dotenv.config();

// CRUD

/**
 * Method to obtain all Users from Collection "Users" in Mongo Server
 */
export const GetAllUsers = async (page: number, limit: number): Promise<any[] | undefined> => {
    try {
        let userModel = userEntity();

        let response: any = {};

        await userModel.find({ isDelete: false }).select('name role email age katas').limit(limit).skip((page - 1) * limit)
            .exec().then((users: IUser[]) => {

                response.users = users;
            });

        await userModel.countDocuments().then((total: number) => {
            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        });

        return response;
    } catch (error) {
        LogError(`[ORM ERROR]: Getting All Users: ${error}`);
    }

}

export const GetUsersForCheck = async (): Promise<any[] | undefined> => {
  try {
      let userModel = userEntity();
      return await userModel.find({ isDelete: false })
  } catch (error) {
      LogError(`[ORM ERROR]: Getting All Users: ${error}`);
  }

}

/* export const GetKatasFromUser = async (page: number, limit: number, id: string): Promise<any[] | undefined> => {
    try {
        let userModel = userEntity();
        let kataModel = kataEntity();

        let katasFound: IKata[] = [];

        let response: any = {
            katas: []
        };

        await userModel.findById(id).then(async (user: IUser) => {
            response.user = user.email;

            let objectIds: mongoose.Types.ObjectId[] = [];
            user.katas.forEach((kataID: string) =>{
                let objectID = new mongoose.Types.ObjectId(kataID)
                objectIds.push(objectID)
            })
            

            await kataModel.find({ "_id": { "$in": objectIds } }).then((katas: IKata[]) => {
                katasFound = katas;
            })
        }).catch((error) => {
            LogError('[ORM ERROR]: Obtaining user error')
        })

        response.katas = katasFound

        return response;
    } catch (error) {
        LogError(`[ORM ERROR]: Getting All Users: ${error}`);
    }

} */

export const GetUserByID = async (id: string): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        return await userModel.findById(id).select('name role email age katas');
    } catch (error) {
        LogError(`[ORM ERROR]: Getting User by ID: ${error}`);
    }
}

export const deleteUserByID = async (id: string): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        return await userModel.deleteOne({ _id: id });
    } catch (error) {
        LogError(`[ORM ERROR]: Deleting User by ID: ${error}`);
    }
}

export const updateUserByID = async (id: string, user: IUser): Promise<any | undefined> => {
    try {
        let userModel = userEntity();
        return await userModel.findByIdAndUpdate(id, user)
    } catch (error) {
        LogError(`[ORM ERROR]: Updating User: ${error}`);
    }

}


