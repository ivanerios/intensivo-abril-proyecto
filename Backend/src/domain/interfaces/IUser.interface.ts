import { IKata } from "./IKata.interface";

export enum UserRole {
  USER = 'User',
  ADMIN = 'Admin'
}

export interface IUser {
    name: string,
    role: UserRole,
    email: string,
    age: number,
    password: string,
    katas: string[]
}