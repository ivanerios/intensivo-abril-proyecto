import express, { Request, Response } from "express";
import { UserController } from "../controller/UserController";
import { LogInfo } from "../utils/logger";
import { IUser, UserRole } from "../domain/interfaces/IUser.interface";

import bcrypt from 'bcrypt';

import bodyParser from "body-parser";
import { verifyToken } from "../middlewares/verifyToken";

let jsonParser = bodyParser.json();

// Router from express

let userRouter = express.Router();

userRouter.route('/')
  .get(async (req: Request, res: Response) => {

    let id: any = req?.query?.id;
    let page: any = req?.query?.page || 1;
    let limit: any = req?.query?.limit || 10;

    LogInfo(`Query Param: ${id}`);

    const controller: UserController = new UserController();
    const response = await controller.getUsers(page, limit, id);

    return res.status(200).send(response);
  })

  .delete(verifyToken, async (req: Request, res: Response) => {

    let id: any = req?.query?.id;
    LogInfo(`Query Param: ${id}`);

    const controller: UserController = new UserController();
    const response = await controller.deleteUser(id);

    return res.status(response.status).send(response);

  })

  .put(verifyToken, jsonParser, async (req: Request, res: Response) => {
    let id: any = req?.query?.id;

    let name: string = req?.body?.name;
    let role: UserRole = req?.body?.role || UserRole.USER;
    let email: string = req?.body?.email || '';
    let age: number = req?.body?.intents || 18;
    let password: string = req?.body?.password || '';
    let katas: [] = req?.body?.stars || [''];

    if (name && role && email && password && age) {
    let hashedPassword = '';

    hashedPassword = bcrypt.hashSync(password, 8);

    const controller: UserController = new UserController();

    let user: IUser = {
      name,
      role,
      email,
      password: hashedPassword,
      age,
      katas
    }

    const response = await controller.updateUser(id, user);

    return res.status(response.status).send(response);} else {
      return res.status(400).send({
        message: '[ERROR User Data missing] No user can be updated'
      });
    }

  });

/* userRouter.route('/katas')
  .get(verifyToken, async (req: Request, res: Response) => {
    let id: any = req?.query?.id;
    let page: any = req?.query?.page || 1;
    let limit: any = req?.query?.limit || 10;


    const controller: UserController = new UserController();
    const response = await controller.getKatas(page, limit, id);

    return res.status(200).send(response);
  }); */

export default userRouter;