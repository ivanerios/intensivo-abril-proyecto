import express, { Request, Response } from 'express';
import helloRouter from './HelloRouter';
import { LogInfo } from '../utils/logger';
import userRouter from './UserRouter';
import authRouter from './AuthRouter';
import kataRouter from './KataRouter';

let server = express();

let rootRouter = express.Router();

rootRouter.get('/', (req: Request, res: Response) => {
    LogInfo('GET: http://localhost:8000/api/')
    //Send Hello World
    res.send('Welcome to Api Restful Express + TS + Swagger + Mongoose');
});

server.use('/', rootRouter);
server.use('/hello', helloRouter);
server.use('/users', userRouter);
server.use('/auth', authRouter);
server.use('/katas', kataRouter);

export default server;