import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';

import dotenv from 'dotenv';

dotenv.config()

const secret = process.env.SECRETKEY || 'MYSECRETKEY';

export const verifyToken = (req: Request, res: Response, next: NextFunction) => {
    let token: any = req.headers['x-access-token'];

    if(!token){
return res.status(403).send({
    authenticationError: 'Missing JWT in request',
    message: 'Not authorised to consume this endpoint'
})
    }
    jwt.verify(token, secret, (err: any, decoded: any) =>{
        if(err){
            return res.status(500).send({
                authenticationError: 'JWT verification failed',
                message: 'Failde to verifty JWT token in request'
            })
        }

        next();
    })
}