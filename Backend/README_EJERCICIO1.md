# Explicación del ejercicio 1

## Dependencias instaladas

Dependencias de producción:

- Express: se trata de un framework basado en Node.js para construir microservicios y servidores de forma rápida, destaca por su extructura escalable y minimalista.

- Dotenv: Modulo encargado de ejecutar variables de entorno a partir de un archivo .env.

Dependencias de desarrollo:

- Typescript: Superconjunto de Javascript que incorpora el tipado fuerte y el uso de interfaces, destinado a hacer el desarrollo en Javascript más ordenado.

- Webpack: Empaquetador de módulos para Javascript, destinado a compactar toda una aplicación en un único .js altamente optimizado.

- Eslint: Normas de escritura de código, según un estandard.

- Jest: Módulo de testing unitario para Javascript.

- Nodemon: Módulo de Node que permite hacer de un servidor en marcha reactivo y automáticamente actualizado a los cambios durante el desarrollo.

- Concurrency: Paquete destinado a poder ejecutar distintos comandos de forma concurrente.

## Scripts creados

- Build: Transpila el código en Typescript a una versión "final" en Javascript.

- Start: Arranca el servidor de Express.

- Dev: Ejecuta los dos anteriores, haciendo el arranque del servidor con Nodemon.

- Test: Arranca los tests que haya escritos con Jest.

- Serve:coverage: Arranca los tests y informa del porcentaje de código de nuestra App cubierto por los citados tests.

## Variables de entorno

- Al punto que estamos solo está definido el puerto en el que se ejecutará la aplicación.