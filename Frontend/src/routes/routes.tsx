import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import { useSessionStorage } from '../hooks/useSessionStorage';
import { WelcomePage } from '../pages/WelcomePage';
import { LoginPage } from '../pages/LoginPage';
import { RegisterPage } from '../pages/RegisterPage';
import { KatasDetailPage } from '../pages/KatasDetailPage';
import { KatasPage } from '../pages/KatasPages';
import { CreateUserPage } from '../pages/CreateUserPage';
import { CreateKataPage } from '../pages/CreateKataPage';
import { UpdateUserPage } from '../pages/UpdateUserPage';
import { UserListPage } from '../pages/UserListPage';
import { UpdateKataPage } from '../pages/UpdateKataPage';
import { RankingPage } from '../pages/RankingPage';
import { DeleteKataSuccesful } from '../pages/DeleteKataSuccesful';
import { DeleteUserSuccesful } from '../pages/DeleteUserSuccesful';

export const AppRoutes = () => {

  let loggedIn = useSessionStorage('sessionJWTToken');

    return (
        <Routes>          
        <Route path='/' element={<WelcomePage />}></Route>
        <Route path='/login' element={<LoginPage />}></Route>
        <Route path='/register' element={<RegisterPage />}></Route>
        <Route path='/katas' element={<KatasPage />}></Route>
        <Route path='/katas/:id' element={<KatasDetailPage />}></Route>
                  <Route path='/users' element={<UserListPage />}></Route>
                  <Route path='/create/user' element={<CreateUserPage />}></Route>
                  <Route path='/create/kata' element={<CreateKataPage />}></Route>
                  <Route path='/update/user/:id' element={<UpdateUserPage />}></Route>
                  <Route path='/update/kata/:id' element={<UpdateKataPage />}></Route>
                  <Route path='/ranking' element={<RankingPage />}></Route>
                  <Route path='/delete/kata' element={<DeleteKataSuccesful />}></Route>
                  <Route path='/delete/user' element={<DeleteUserSuccesful />}></Route>

        <Route path='*' element={<Navigate to='/' replace />}></Route>
      </Routes>
    )
}