export type User = {
  _id: string,
  name: string,
  email: string,
  role: string,
  age: number,
  password: string,
  katas: []
}

export type UserValues = {
  name: string | undefined,
  email: string | undefined,
  age: number | undefined
}