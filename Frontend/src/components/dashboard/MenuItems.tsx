import React, { useContext, useEffect, useState } from 'react';

import MyContext from '../../context/MyContext';

import { ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import { People, EmojiEvents, SportsMartialArts } from '@mui/icons-material';
import { Link } from 'react-router-dom';

export const MenuItems = () => {

  const { userInfo, setUserInfo } = useContext(MyContext)

  const [isAdmin, setIsAdmin] = useState<boolean>(false)
  
  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  },[isAdmin])
  return (
    <>
    <Link to="/katas"><ListItemButton>
        <ListItemIcon>
        <SportsMartialArts />
        </ListItemIcon>
        <ListItemText primary="Katas" />
    </ListItemButton></Link>
    {isAdmin ? <Link to="/users"> <ListItemButton>
        <ListItemIcon>
            <People />
        </ListItemIcon>
        <ListItemText primary="Users" />
    </ListItemButton></Link> : null }
    
    <Link to="/ranking"> <ListItemButton>
        <ListItemIcon>
            <EmojiEvents />
        </ListItemIcon>
        <ListItemText primary="Ranking" />
    </ListItemButton></Link>
    </>)
}