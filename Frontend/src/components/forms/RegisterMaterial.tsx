import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import CssBaseline from '@mui/material/CssBaseline';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { lightGreen } from '@mui/material/colors';
import RegisterForm from './RegisterForm';

import logo from '../../img/CODO_LOGO.svg'
import { Paper } from '@mui/material';


const theme = createTheme({
  palette: {
    background: {
      default: "#555555"
    },
    primary: lightGreen,
    secondary: {
      main: '#555555',
    },
  },
});

export const RegisterMaterial = () => {


  return (
    <ThemeProvider theme={theme}>
      <Container component={Paper} maxWidth="xs" sx={{ mt: '10vh', height: '80vh' }}>
        <CssBaseline />
        <Box 
          sx={{ 
            paddingTop: 5,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <img style={{width: "250px", marginBottom: "20px"}} src={logo} />
          <Avatar sx={{ m: 1, bgcolor: 'white' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Register
          </Typography>
          <Box sx={{ mt: 3 }}>
            <Grid container>
              <RegisterForm />
              </Grid>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link href="/login" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}