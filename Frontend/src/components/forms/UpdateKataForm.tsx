import React, { useContext, useEffect, useState } from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';

import { register } from "../../services/authService";
import { AxiosResponse } from "axios";
import { Box, Button, FormControl, InputLabel, MenuItem, Select, TextField } from "@mui/material";

import axios from '../../utils/config/axios.config';
import { useNavigate, useParams } from "react-router-dom";
import { Kata } from "../../utils/types/Kata.type";
import { useSessionStorage } from "../../hooks/useSessionStorage";
import MyContext from "../../context/MyContext";
import { createKata, getKataByID, updateKataByID } from "../../services/katasService";

const UpdateKataForm = () => {

  let loggedIn = useSessionStorage('sessionJWTToken');

  let { id } = useParams();

  const [kata, setKata] = useState<Kata | undefined>(undefined);

  useEffect(() => {
    if (!loggedIn) {
      return navigate('/login');
    } else {
      if (id) {
        getKataByID(loggedIn, id).then((response: AxiosResponse) => {
          if (response.status === 200 && response.data) {
            let kataData: Kata = {
              _id: response.data._id,
              name: response.data.name,
              description: response.data.description,
              level: response.data.level,
              intents: response.data.intents,
              stars: response.data.stars,
              creator: response.data.creator,
              solution: response.data.solution,
              participants: response.data.participants
            }

            setKata(kataData);
          }

        }).catch((error) => console.error('Kata By ID error'))
      } else {

      }

    }
  }, [loggedIn])

  const { userInfo, setUserInfo } = useContext(MyContext)

  const initialValues = {
    _id: kata?._id,
    name: kata?.name,
    description: kata?.description,
    level: kata?.level,
    intents: kata?.intents,
    stars: kata?.stars,
    creator: kata?.creator,
    solution: kata?.solution,
    participants: kata?.participants
  }

  let navigate = useNavigate();

  //Yup Validation Schema
  const registerSchema = Yup.object().shape(
    {
      name: Yup.string().min(6, 'The name of the kata must have 6 letters minimun').required('Kata name is required'),
      description: Yup.string().min(6, 'The description of the kata must have 6 letters minimun').required('Description is required'),
      level: Yup.string(),
      solution: Yup.string().min(8, 'The solution of the kata must have 6 letters minimun').required('Solution is required'),
    }
  );

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={registerSchema}
        onSubmit={async (values) => {
          updateKataByID(loggedIn, values._id, values.name, values.description, values.level, values.intents, values.stars, values.creator, values.solution, values.participants).then((response: AxiosResponse) => {
            if (response.status == 200) {
              alert('Kata updated correctly')
              if(!loggedIn){
              navigate('/login')
            }else{navigate('/katas')}
            } else {
              throw new Error('Error in update')
            }
          }).catch((error) => console.error('Update Error')
          )
        }}
      >

        {
          ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
            <Form>

              <TextField
                fullWidth
                InputLabelProps={{ shrink: true }}
                id='name' label='name' type='name' name='name'
                value={values.name}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.name && touched.name && (
                  <ErrorMessage className="error" name='name' component='div'></ErrorMessage>
                )
              }


              <TextField
                sx={{
                  mt: 1,
                  mb: 1
                }}
                fullWidth
                multiline
                rows={6}
                InputLabelProps={{ shrink: true }}
                id='description' label='description' type='description' name='description'
                value={values.description}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.description && touched.description && (
                  <ErrorMessage className="error" name='description' component='div'></ErrorMessage>
                )
              }

              <FormControl fullWidth>
                <InputLabel id="level-label">level</InputLabel>
                <Select sx={{
                  mt: 1,
                  mb: 1
                }}
                  fullWidth
                  labelId="level-label"
                  id='level'
                  defaultValue={values.level} label='level'
                  onChange={handleChange}>
                  <MenuItem value={'Basic'}>Basic</MenuItem>
                  <MenuItem value={'Medium'}>Medium</MenuItem>
                  <MenuItem value={'High'}>High</MenuItem>
                </Select>

              </FormControl>


              <TextField
                fullWidth
                multiline
                rows={6}
                InputLabelProps={{ shrink: true }}
                id='solution' label='solution' type='solution' name='solution'
                value={values.solution}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.solution && touched.solution && (
                  <ErrorMessage className="error" name='solution' component='div'></ErrorMessage>
                )
              }

              <Button sx={{
                mt: 1,
                mb: 1
              }} fullWidth color="primary" variant="contained" type='submit'>Update Kata</Button>
              {
                isSubmitting ? (<p>Sending Data to Registry...</p>) : null
              }
            </Form>
          )
        }


      </Formik>
    </div>
  )
}


export default UpdateKataForm