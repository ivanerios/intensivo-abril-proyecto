import React, { useContext, useEffect, useState } from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from 'yup';

import { register } from "../../services/authService";
import { AxiosResponse } from "axios";
import { Box, Button, FormControl, InputLabel, MenuItem, Select, TextField } from "@mui/material";

import axios from '../../utils/config/axios.config';
import { useNavigate } from "react-router-dom";
import { useSessionStorage } from "../../hooks/useSessionStorage";
import MyContext from "../../context/MyContext";
import { createKata } from "../../services/katasService";

const CreateKataForm = () => {

  let loggedIn = useSessionStorage('sessionJWTToken');

  const { userInfo, setUserInfo } = useContext(MyContext)

  const initialValues = {
    name: '',
    description: '',
    level: 'Basic',
    intents: 1,
    stars: 1,
    creator: userInfo._id,
    solution: '',
    participants: ['']
  }

  let navigate = useNavigate();

  //Yup Validation Schema
  const registerSchema = Yup.object().shape(
    {
      name: Yup.string().min(6, 'The name of the kata must have 6 letters minimun').required('Kata name is required'),
      description: Yup.string().min(6, 'The description of the kata must have 6 letters minimun').required('Description is required'),
      level: Yup.string(),
      solution: Yup.string().min(8, 'The solution of the kata must have 6 letters minimun').required('Solution is required'),
    }
  );

  return (
    <div>
      <Formik
      enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={registerSchema}
        onSubmit={async (values) => {
          createKata(loggedIn, values.name, values.description, values.level, values.intents, values.stars, values.creator, values.solution, values.participants).then((response: AxiosResponse) => {
            if (response.status == 200) {
              alert('Kata created correctly')
              if(!loggedIn){
              navigate('/login')
            }else{navigate('/katas')}
            } else {
              throw new Error('Error in creation')
            }
          }).catch((error) => console.error('Creation Error')
          )
        }}
      >

        {
          ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
            <Form>

              <TextField
                fullWidth
                id='name' label='name' type='name' name='name'
                value={values.name}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.name && touched.name && (
                  <ErrorMessage className="error" name='name' component='div'></ErrorMessage>
                )
              }

              
              <TextField
                sx={{
                  mt: 1,
                  mb: 1
                }}
                multiline
                rows={6}
                fullWidth
                id='description' label='description' type='description' name='description'
                value={values.description}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.description && touched.description && (
                  <ErrorMessage className="error" name='description' component='div'></ErrorMessage>
                )
              }

<FormControl fullWidth>
                <InputLabel id="level-label">level</InputLabel>
                <Select sx={{
                  mt: 1,
                  mb: 1
                }}
                  fullWidth
                  labelId="level-label"
                  id='level'
                  defaultValue={values.level} label='level'
                  onChange={handleChange}>
                  <MenuItem value={'Basic'}>Basic</MenuItem>
                  <MenuItem value={'Medium'}>Medium</MenuItem>
                  <MenuItem value={'High'}>High</MenuItem>
                </Select>

              </FormControl>


              <TextField
                fullWidth
                multiline
                rows={6}
                id='solution' label='solution' type='solution' name='solution'
                value={values.solution}
                onChange={handleChange} onBlur={handleBlur} />
              {
                errors.solution && touched.solution && (
                  <ErrorMessage className="error" name='solution' component='div'></ErrorMessage>
                )
              }

                  <Button sx={{
                mt: 1,
                mb: 1
              }} fullWidth color="primary" variant="contained" type='submit'>Create Kata</Button>
              {
                isSubmitting ? (<p>Sending Data to Registry...</p>) : null
              }
            </Form>
          )
        }


      </Formik>
    </div>
  )
}


export default CreateKataForm