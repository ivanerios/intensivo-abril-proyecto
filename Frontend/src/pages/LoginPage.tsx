import react, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useSessionStorage } from "../hooks/useSessionStorage";
import { LoginMaterial } from "../components/forms/LoginMaterial";

export const LoginPage = () => {

  let loggedIn = useSessionStorage('sessionJWTToken');
  let navigate = useNavigate();
  useEffect(() => {
    if (loggedIn) {
      return navigate('/');
    }
  }, [loggedIn])
  return (
    <div>
      <LoginMaterial />
    </div>
  )
}