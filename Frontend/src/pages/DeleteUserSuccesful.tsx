import { CircularProgress, createTheme } from "@mui/material";
import { lightGreen } from "@mui/material/colors";
import react, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";

import logo from '../img/CODO_LOGO.svg'

const myTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: {
      main: '#FFFFFF',
    },
  },
});

export const DeleteUserSuccesful = () => {

  let navigate = useNavigate();

useEffect(() => {
  navigate('/users')
}, [])

    return (
        <div>
          <p><img style={{width: "250px", marginTop: "40vh"}} src={logo} /></p>
            <CircularProgress color="primary" />
        </div>
    )
}