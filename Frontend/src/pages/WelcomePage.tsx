import { AxiosResponse } from "axios";
import react, { useContext, useEffect, useState } from "react";

import { Link, useNavigate, useParams } from "react-router-dom";
import MyContext from "../context/MyContext";

import { createTheme, ThemeProvider, Box, Toolbar, Container, Grid, Paper, Typography, IconButton, Divider, TableContainer, Table, TableBody, TableRow, TableCell } from '@mui/material';
import { lightGreen } from '@mui/material/colors';
import { MenuDashboard } from '../components/dashboard/MenuDashboard';

import { useSessionStorage } from "../hooks/useSessionStorage";
import { getAllUsers, getUserByID } from "../services/usersService";
import { User } from "../utils/types/User.type";
import { Kata } from "../utils/types/Kata.type";
import { getKataByID } from "../services/katasService";
import { EmojiEvents, People, Person, SportsMartialArts } from "@mui/icons-material";

import logo from '../img/CODO_LOGO.svg'

const myTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: {
      main: '#FFFFFF',
    },
  },
});

export const WelcomePage = () => {

  const { userInfo, setUserInfo } = useContext(MyContext)

  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response: AxiosResponse) => {
      await setUserInfo({_id: response.data._id,
      name: response.data.name,
      role: response.data.role,
      email: response.data.email,
      age: response.data.age,
      katas: response.data.katas})})
  }, [])
  

  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');
  let navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState<boolean>(false)

  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  }, [isAdmin])

  let { id } = useParams();

  const [kata, setKata] = useState<Kata | undefined>(undefined);

  useEffect(() => {
    if (!loggedIn) {
      return navigate('/login');
    } else {
      if (id) {
        getKataByID(loggedIn, id).then((response: AxiosResponse) => {
          if (response.status === 200 && response.data) {
            let kataData: Kata = {
              _id: response.data._id,
              name: response.data.name,
              description: response.data.description,
              stars: response.data.stars,
              level: response.data.level,
              intents: response.data.intents,
              creator: response.data.creator,
              solution: response.data.solution,
              participants: response.data.participants
            }

            setKata(kataData);

            console.table(kata)
          }

        }).catch((error) => console.error('Kata By ID error'))
      } else {

      }

    }
  }, [loggedIn])


  return (
    <ThemeProvider theme={myTheme}>
      <Box sx={{ display: 'flex' }}>
        <MenuDashboard />
        <Box component='main' sx={{ backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900], flexGrow: 1, height: '100vh', overflow: 'auto' }}>
          <Toolbar />
          <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
            <Grid item xs={12} md={8} lg={9}>
              <TableContainer component={Paper} sx={{ p: 2, display: 'flex', flexDirection: 'column', height: '85vh' }}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableBody>
                    <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
                      <TableCell align="center">
                        <p><img style={{ width: "250px"}} src={logo} /></p>
                      </TableCell>
                    </TableRow>
                    <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
                      <TableCell align="center">
                        <Typography component='h6' variant='h6' color='inherit' noWrap sx={{ marginTop: '15px', marginBottom: '15px' }}>Welcome {userInfo.name}, It's nice to see you</Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
                      <TableCell align="center">
                        {userInfo.role === 'Admin'? <Link to='/users'><IconButton color='primary'><People sx={{ fontSize: '50px', display: 'inline' }} /></IconButton></Link> : null }
                        <Link to='/katas'><IconButton color='primary'><SportsMartialArts sx={{ fontSize: '50px', display: 'inline' }} /></IconButton></Link>
                        <Link to='/ranking'><IconButton color='primary'><EmojiEvents sx={{ fontSize: '50px', display: 'inline' }} /></IconButton></Link>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>

            </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  )
}