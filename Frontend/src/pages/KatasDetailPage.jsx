import { AxiosResponse } from "axios";
import react, { useContext, useEffect, useState, Fragment } from "react";

import Editor from 'react-simple-code-editor';
import Highlight, { defaultProps, Language } from 'prism-react-renderer';
import theme from 'prism-react-renderer/themes/nightOwl';

import { useNavigate, useParams } from 'react-router-dom';
import MyContext from "../context/MyContext";

import { createTheme, ThemeProvider, Box, Toolbar, Container, Grid, Paper, Button, TextField, IconButton } from '@mui/material';
import { lightGreen } from '@mui/material/colors';
import { MenuDashboard } from '../components/dashboard/MenuDashboard';

import { useSessionStorage } from "../hooks/useSessionStorage";
import { getKataByID, updateKataByID } from "../services/katasService";
import { Kata } from "../utils/types/Kata.type";
import { Star } from "@mui/icons-material";

import { getUserByID } from "../services/usersService";

const styles = {
  root: {
    boxSizing: 'border-box',
    fontFamily: '"Dank Mono", "Fira Code", monospace',
    ...theme.plain
  }
}

const languages = [
  "tsx",
  "typescript",
  "javascript",
  "jsx",
  "python",
  "json",
  "go"
]

const HighlightElement = (code) => (
  <Highlight {...defaultProps} theme={theme} code={code} language='jsx'>
    {({ className, style, tokens, getLineProps, getTokenProps }) => (
      <Fragment>
        {tokens.map((line, i) => (
          <div {...getLineProps({ line, key: i })}>
            {line.map((token, key) => <span {...getTokenProps({ token, key })} />)}
          </div>
        ))}
      </Fragment>
    )}
  </Highlight>
);

const myTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: {
      main: '#FFFFFF',
    },
  },
});

export const KatasDetailPage = () => {

  const [code, setCode] = useState('');

  const [languageSelected, setLanguageSelected] = useState(languages[1]);

  const styles = {
    root: {
      boxSizing: 'border-box',
      fontFamily: '"Dank Mono", "Fira Code", monospace',
      ...theme.plain
    }
  }

  const handleLanguageChange = (newValue) => {
    setLanguageSelected(newValue);
  }

  const handleChange = (newCode) => {
    setCode(newCode);
  }

  const { userInfo, setUserInfo } = useContext(MyContext)

  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');
  let navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState(false)

  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  }, [isAdmin])

  let { id } = useParams();

  let kataInitialValues = {
    _id: '',
    name: '',
    description: '',
    stars: 1,
    level: '',
    intents: 0,
    creator: '',
    solution: '',
    participants: ['']
  }

  const [kata, setKata] = useState(kataInitialValues);

  const [showSolution, setShowSolution] = useState(false)

  useEffect(() => {
    if (!loggedIn) {
      return navigate('/login');
    } else {
      if (id) {
        getKataByID(loggedIn, id).then((response) => {
          if (response.status === 200 && response.data) {
            let kataData = {
              _id: response.data._id,
              name: response.data.name,
              description: response.data.description,
              stars: response.data.stars,
              level: response.data.level,
              intents: response.data.intents,
              creator: response.data.creator,
              solution: response.data.solution,
              participants: response.data.participants
            }

            setKata(kataData);

            setCode(kataData.description);
          }

        }).catch((error) => console.error('Kata By ID error'))
      } else {
        return navigate('/katas');
      }

    }
  }, [loggedIn])

  const [stars, setStars] = useState(1)

  const [starsCount, setStarsCount] = useState(1)

  const [isRated, setIsRated] = useState(false)

  const starsIncrement = () => {
    if (stars < 5) {
      setStars(stars + 1)
    } else {
      setStars(1)
    }
  }

  const showTheSolution = () => {

    setShowSolution(true)

    setKata({
      ...kata,
      intents: kata.intents + 1
    })

    if (kata.stars < 1) {
      setKata({
        ...kata,
        stars: kata.stars + 1
      })
    }


    if (!Object.values(kata.participants).includes(userInfo._id)) {
      setKata({
        ...kata,
        participants: [...kata.participants, userInfo._id]
      });
    }
  }

  const rateTheKata = () => {

    let multi = kata.stars * kata.intents;
    let sume = multi + stars;
    let divider = kata.intents + 1;
    let result = sume / divider;

    console.log(result)
    
    setKata({
      ...kata,
      stars: result
    });

    setIsRated(true)
  }

  const sendResults = () => {
    updateKataByID(loggedIn, kata._id, kata.name, kata.description, kata.level, kata.intents, kata.stars, kata.creator, kata.solution, kata.participants)
  }
  
  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response) => {
      await setUserInfo({_id: response.data._id,
      name: response.data.name,
      role: response.data.role,
      email: response.data.email,
      age: response.data.age,
      katas: response.data.katas})})
  }, [])

  return (
    <ThemeProvider theme={myTheme}>
      <Box sx={{ display: 'flex' }}>
        <MenuDashboard />
        <Box component='main' sx={{ backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900], flexGrow: 1, height: '100vh', overflow: 'auto' }}>
          <Toolbar />
          <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
            <Grid item xs={12} md={8} lg={9}>
              <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column', height: '85vh' }}>
                {kata ?
                  <div className="kata-data">
                    <Editor value={code} onValueChange={handleChange} highlight={HighlightElement} padding={10} style={styles.root} />
                    <h3>Rating: {starsCount}/5</h3>
                    {!showSolution ? <Button sx={{
                      mt: 1,
                      mb: 1
                    }} fullWidth color="primary" variant="contained" onClick={() => showTheSolution()}>Check Solution</Button> : null}

                    {showSolution ?
                      <>
                        <Highlight {...defaultProps} code={kata.solution} language='jsx'>
                          {({ className, style, tokens, getLineProps, getTokenProps }) => (
                            <pre className={className} style={styles.root}>
                              {tokens.map((line, i) => (
                                <div {...getLineProps({ line, key: i })}>
                                  {line.map((token, key) => (
                                    <span {...getTokenProps({ token, key })} />
                                  ))}
                                </div>
                              ))}
                            </pre>
                          )}
                        </Highlight>
                        <span>{stars}</span><IconButton color='primary' onClick={starsIncrement}><Star /></IconButton>
                        {!isRated ? <Button sx={{
                          mt: 1,
                          mb: 1
                        }} fullWidth color="primary" variant="contained" onClick={rateTheKata}>Rate the Kata</Button> : <Button sx={{
                          mt: 1,
                          mb: 1
                        }} fullWidth color="primary" variant="contained" onClick={sendResults}>Send your result</Button>}
                      </>
                      : null}
                  </div> : <div>
                    <h2>Loading Data</h2></div>}
              </Paper>
            </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  )
}