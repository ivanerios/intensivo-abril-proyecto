import { AxiosResponse } from "axios";
import react, { useContext, useEffect, useState } from "react";

import { useNavigate, useParams } from "react-router-dom";
import MyContext from "../context/MyContext";

import { createTheme, ThemeProvider, Box, Toolbar, Container, Grid, Paper } from '@mui/material';
import { lightGreen } from '@mui/material/colors';
import { MenuDashboard } from '../components/dashboard/MenuDashboard';

import { useSessionStorage } from "../hooks/useSessionStorage";
import { getUserByID } from "../services/usersService";
import { User } from "../utils/types/User.type";
import UpdateUserForm from "../components/forms/UpdateUserForm";

const myTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: {
      main: '#FFFFFF',
    },
  },
});

export const UpdateUserPage = () => {

  const { userInfo, setUserInfo } = useContext(MyContext)

  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');
  let navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState<boolean>(false)
  
  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  },[isAdmin])

  let { id } = useParams();

  const [user, setUser] = useState<User | undefined>(undefined);

    useEffect(() =>{
      if(!loggedIn){
          return navigate('/login'); 
      }else{
          if(id){
              getUserByID(loggedIn, id).then((response: AxiosResponse ) =>{
                  if(response.status === 200 && response.data){
                      let userData: User = {
                          _id: response.data._id,
                          name: response.data.name,
                          role: response.data.role,
                          email: response.data.email,
                          age: response.data.age,
                          password: response.data.password,
                          katas: response.data.katas
                      }

                      setUser(userData);

                      console.table(user)
                  }

          }).catch((error) => console.error('User By ID error')) 
          }else{
              return navigate('/users');
          }
         
      }
  }, [loggedIn])

  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response: AxiosResponse) => {
      await setUserInfo({_id: response.data._id,
      name: response.data.name,
      role: response.data.role,
      email: response.data.email,
      age: response.data.age,
      katas: response.data.katas})})
  }, [])

    return (
      <ThemeProvider theme={myTheme}>
      <Box sx={{ display: 'flex' }}>
      <MenuDashboard />
        <Box component='main' sx={{ backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900], flexGrow: 1, height: '100vh', overflow: 'auto' }}>
          <Toolbar />
          <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
            <Grid item xs={12} md={8} lg={9}>
              <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column', height: '85vh' }}>
                <UpdateUserForm />
            
            </Paper>
            </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
    )
}