import { AxiosResponse } from "axios";
import react, { useContext, useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";
import MyContext from "../context/MyContext";

import { createTheme, ThemeProvider, Box, Toolbar, Container, Grid, Paper, TableContainer, Table, TableBody, IconButton, TableRow, TableCell, Badge, Button } from '@mui/material';
import { lightGreen } from '@mui/material/colors';
import { MenuDashboard } from '../components/dashboard/MenuDashboard';

import { useSessionStorage } from "../hooks/useSessionStorage";
import { getAllKatas, getAllKatasSorted } from "../services/katasService";
import { Kata } from "../utils/types/Kata.type";
import { AddCircle, ChevronLeft, ChevronRight, Delete, Edit, Person, SportsMartialArts, Star } from "@mui/icons-material";
import { getUserByID } from "../services/usersService";

const myTheme = createTheme({
  palette: {
    primary: lightGreen,
    secondary: {
      main: '#FFFFFF',
    },
  },
});

export const RankingPage = () => {

  const { userInfo, setUserInfo } = useContext(MyContext)

  let loggedIn = useSessionStorage('sessionJWTToken');
  let sessionIn = useSessionStorage('userId');
  let navigate = useNavigate();

  const [isAdmin, setIsAdmin] = useState<boolean>(false)

  useEffect(() => {
    userInfo.role === 'Admin' ? setIsAdmin(true) : setIsAdmin(false)
  }, [isAdmin])

  const [katas, setKatas] = useState([]);
  const [currentPageNumber, setCurrentPageNumber] = useState(1)
  const [totalPages, setTotalPages] = useState(1)

  const previousPage = () => {
    setCurrentPageNumber(currentPageNumber - 1);
  }

  const nextPage = () => {
    setCurrentPageNumber(currentPageNumber + 1)
  }

  useEffect(() => {
    getKatasByIntents();
  }, [currentPageNumber])

  useEffect(() => {
    if (!loggedIn) {
      return navigate('/login');
    } else {
      getKatasByIntents()
    }
  }, [loggedIn])

  const navigateToKataDetail = (id: string) => {
    navigate(`/katas/${id}`)
  }

  const navigateToKataUpdate = (id: string) => {
    navigate(`/update/kata/${id}`)
  }

  const navigateToKataCreate = () => {
    navigate('/create/kata')
  }

  const getKatasByIntents = () => {
    getAllKatasSorted(loggedIn, 5, currentPageNumber, 'intents').then((response: AxiosResponse) => {
      if (response.status === 200 && response.data.katas && response.data.currentPage && response.data.totalPages) {
        let { katas, totalPages } = response.data
        setKatas(katas);
        setTotalPages(totalPages);
      } else {
        throw new Error('Error obtaining Katas')
      }
    }).catch((error) => console.error('Get All Users Error'))
  }

  const getKatasByStars = () => {
    getAllKatasSorted(loggedIn, 5, currentPageNumber, 'stars').then((response: AxiosResponse) => {
      if (response.status === 200 && response.data.katas && response.data.currentPage && response.data.totalPages) {
        let { katas, totalPages } = response.data
        setKatas(katas);
        setTotalPages(totalPages);
      } else {
        throw new Error('Error obtaining Katas')
      }
    }).catch((error) => console.error('Get All Users Error'))
  }

  useEffect(() => {
    getUserByID(loggedIn, sessionIn).then(async (response: AxiosResponse) => {
      await setUserInfo({_id: response.data._id,
      name: response.data.name,
      role: response.data.role,
      email: response.data.email,
      age: response.data.age,
      katas: response.data.katas})})
  }, [])

  return (
    <ThemeProvider theme={myTheme}>
      <Box sx={{ display: 'flex' }}>
        <MenuDashboard />
        <Box component='main' sx={{ backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900], flexGrow: 1, height: '100vh', overflow: 'auto' }}>
          <Toolbar />
          <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
            <Grid item xs={12} md={8} lg={9}>
            <Button sx={{ mt: 1, mb: 1 }} fullWidth color="primary" variant="contained" onClick={getKatasByStars}>Best rated</Button>
              <Button sx={{ mt: 1, mb: 1 }} fullWidth color="primary" variant="contained" onClick={getKatasByIntents}>Most popular</Button>
              <TableContainer component={Paper} sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                <Table sx={{ minWidth: 250 }} aria-label="simple table">
                  {katas.length > 0 ? <TableBody>

                    {katas.map((kata: Kata) => (
                      <TableRow key={kata._id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
                        <TableCell><h3 onClick={() => navigateToKataDetail(kata._id)}>{kata.name}</h3></TableCell>
                        <TableCell align="right"><IconButton color='primary' ><Badge badgeContent={Math.floor(kata.stars)} color='default'><Star /></Badge></IconButton><IconButton color='primary' ><Badge badgeContent={kata.intents} color='default'><SportsMartialArts /></Badge></IconButton></TableCell>
                      </TableRow>
                    ))}

                  </TableBody> : <div><h5>No katas found</h5></div>}
                </Table>
              </TableContainer>
              <span>Page {currentPageNumber}</span>
              {currentPageNumber > 1 ? <IconButton color='inherit' onClick={previousPage}><ChevronLeft /></IconButton> : <IconButton color='inherit'><ChevronLeft /></IconButton>}
              {currentPageNumber < totalPages ? <IconButton color='inherit' onClick={nextPage}><ChevronRight /></IconButton> : <IconButton color='inherit'><ChevronRight /></IconButton>}
              <IconButton onClick={() => navigateToKataCreate()} color='primary'><AddCircle /></IconButton>
            </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  )
}