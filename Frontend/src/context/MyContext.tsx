import { createContext, useState, useContext } from "react";
import { Props, UserType } from "./types";

export const MyContext = createContext<UserType>({
  userInfo: {_id: '',
  name: '',
  role: '',
  email: '',
  age: 0,
  katas: ['']},
  setUserInfo: () => { }
})

export default MyContext